package main

import (
	"flag"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/vicanso/go-charts/v2"
	"os"
)

func main() {
	var path = flag.String("pcap", "my.pcap", "Path to .pcap file")
	flag.Parse()

	var IPHistData = make(map[string]float64)

	if handle, err := pcap.OpenOffline(*path); err != nil {
		panic(err)
	} else {
		packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
		for packet := range packetSource.Packets() {
			handlePacket(packet, IPHistData)
		}
		fmt.Print(IPHistData)
		displayHistogram(IPHistData)
	}
}

func handlePacket(packet gopacket.Packet, IPHistData map[string]float64) {
	if IPlayer := packet.Layer(layers.LayerTypeIPv4); IPlayer != nil {
		ip, _ := IPlayer.(*layers.IPv4)
		IPHistData[ip.DstIP.String()] += 1
		IPHistData[ip.SrcIP.String()] += 1
	} else if IPlayer := packet.Layer(layers.LayerTypeIPv6); IPlayer != nil {
		ip, _ := IPlayer.(*layers.IPv6)
		IPHistData[ip.DstIP.String()] += 1
		IPHistData[ip.SrcIP.String()] += 1
	}
}

func displayHistogram(IPHistData map[string]float64) {
	IPs := Keys(IPHistData)
	IPCounts := Values(IPHistData)
	values := make([][]float64, 1)
	values[0] = make([]float64, len(IPCounts))
	for i, v := range IPCounts {
		values[0][i] = v
	}

	p, err := charts.HorizontalBarRender(
		values,
		charts.TitleTextOptionFunc("IPs"),
		charts.PaddingOptionFunc(charts.Box{
			Top:    0,
			Right:  50,
			Bottom: 0,
			Left:   50,
		}),
		charts.LegendLabelsOptionFunc([]string{
			"count",
		}),
		charts.YAxisDataOptionFunc(IPs),
		charts.HeightOptionFunc(2500),
	)
	if err != nil {
		panic(err)
	}

	buf, err := p.Bytes()
	if err != nil {
		panic(err)
	}

	permissions := 0644
	err = os.WriteFile("horizontal.png", buf, os.FileMode(permissions))
	if err != nil {
		panic(err)
	}
}

func Keys[M ~map[K]V, K comparable, V any](m M) []K {
	r := make([]K, 0, len(m))
	for k := range m {
		r = append(r, k)
	}
	return r
}

func Values[M ~map[K]V, K comparable, V any](m M) []V {
	r := make([]V, 0, len(m))
	for _, v := range m {
		r = append(r, v)
	}
	return r
}
