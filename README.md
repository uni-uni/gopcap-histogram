# Gopcap-histogram

Go project which reads  pcap file and draws chart of IPs and number of packets for each address.

### Requirements:
- go1.18
- libpcap-devel

### How to run:
`go run main.go -pcap=/path/to/pcap/file`
