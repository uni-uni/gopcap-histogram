module gopcap-histogram

go 1.18

require (
	github.com/google/gopacket v1.1.19
	github.com/vicanso/go-charts/v2 v2.6.0
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/wcharczuk/go-chart/v2 v2.1.0 // indirect
	golang.org/x/image v0.0.0-20220902085622-e7cb96979f69 // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
)
